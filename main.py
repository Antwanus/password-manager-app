from tkinter import *
from tkinter import messagebox
import random as r
import json

# ---------------------------- FIND PASSWORD BY WEBSITE-------------------------- #
def find_pwd_by_website():
    website = website_input.get()
    try:
        with open("data.json", "r") as file:
            data_as_dict = json.load(file)
            found = data_as_dict[website]
    except KeyError as e:
        messagebox.showerror(title='Not found', message=f'The website "{e}" was not found')
    except FileNotFoundError as e:
        messagebox.showerror(title='File not found!!', message=f'no file found: {e}')
    else:
        messagebox.showinfo(title='Credentials found!', message=f'{found["user"]} - {found["password"]}')

        if pwd_input.get():
            pwd_input.delete(0, END)
        pwd_input.insert(END, found['password'])

        if username_input.get():
            username_input.delete(0, END)
        username_input.insert(END, found['user'])


# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def generate_password():
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
               'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
               'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    nr_letters = r.randint(8, 10)
    nr_symbols = r.randint(2, 4)
    nr_numbers = r.randint(2, 4)

    password_list = [r.choice(letters) for _ in range(nr_letters)]
    password_list += [r.choice(symbols) for _ in range(nr_symbols)]
    password_list += [r.choice(numbers) for _ in range(nr_numbers)]

    r.shuffle(password_list)

    password = "".join(password_list)
    print(password)

    if pwd_input.get():
        pwd_input.delete(0, END)
    pwd_input.insert(END, password)


# ---------------------------- SAVE PASSWORD ------------------------------- #
def save_credentials():
    website = website_input.get()
    user = username_input.get()
    pwd = pwd_input.get()
    credentials_as_json = {website: {"user": user, "password": pwd}}
    if not website:
        messagebox.showerror(title="Something went wrong...", message="No website was provided")
    elif not user:
        messagebox.showerror(title="Something went wrong...", message="No user was provided")
    elif not pwd:
        messagebox.showerror(title="Something went wrong...", message="No password was provided")
    else:
        is_confirmed = messagebox.askokcancel(title="Save profile for website ?",
                                              message=f"{user} --- {website}")
        if is_confirmed:
            try:
                with open("data.json", "r") as file:
                    data_as_a_dict = json.load(file)
                    print(data_as_a_dict)
            except FileNotFoundError as e:
                print("FileNotFoundError :( ", e)
                with open("data.json", "w") as file:
                    json.dump(credentials_as_json, file, indent=4)
            else:
                data_as_a_dict.update(credentials_as_json)
                with open("data.json", "w") as file:
                    json.dump(data_as_a_dict, file, indent=4)
            finally:
                # RESET UI
                website_input.delete(0, END)
                pwd_input.delete(0, END)


# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Password Manager")
window.config(padx=50, pady=50)

#           0               1                   2
# 0                        LOGO
# 1   website_label     website_input
# 2   username_label    username_input
# 3   pwd_label           password_in      generate_pwd_btn
canvas = Canvas(height=200, width=200)
logo_img = PhotoImage(file="logo.png")
canvas.create_image(100, 100, image=logo_img)
canvas.grid(column=1, row=0)

website_label = Label(text="Website:")
website_label.grid(column=0, row=1)

website_input = Entry(width=32)
website_input.grid(column=1, row=1)
website_input.focus()

username_label = Label(text="Username/email:")
username_label.grid(column=0, row=2)

username_input = Entry(width=52)
username_input.grid(column=1, row=2, columnspan=2)
username_input.insert(END, "antveree@gmail.com")

find_btn = Button(text="Find", command=find_pwd_by_website, width=14)
find_btn.grid(column=2, row=1)

pwd_label = Label(text="Password:")
pwd_label.grid(column=0, row=3)

pwd_input = Entry(width=32)
pwd_input.grid(column=1, row=3)

generate_pwd_btn = Button(text="Generate Password", command=generate_password)
generate_pwd_btn.grid(column=2, row=3)

add_btn = Button(text="Add", width=44, command=save_credentials)
add_btn.grid(column=1, row=5, columnspan=2, padx=5, pady=5)

window.mainloop()
